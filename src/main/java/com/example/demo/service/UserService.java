package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.logic.CipherUtil;
import com.example.demo.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;
	@Autowired
	HttpSession session;

	//ユーザー管理画面から呼ばれる
	//ユーザー情報一括取得
	public List<User> findByUser() {
		return userRepository.findAllByOrderByCreatedDateDesc();
	}

	//ユーザー登録や編集、ステータス変更処理から呼ばれる
	//ユーザー情報を登録する
	public void saveUser(User user) {

		userRepository.save(user);

	}

	//ユーザー編集画面表示処理から呼ばれる
	//指定したIDのユーザーを取得する
	public Optional<User> userFindById(int id) {
		return userRepository.findById(id);
	}

	public Optional<User> findByAccount(String account) {
		return userRepository.findByAccount(account);
	}

	// ユーザーIDとパスワードで検索
	public Optional<User> findByAccountAndPassword(String account, String password) {
		//パスワードの再設定
		String encPassword = CipherUtil.encrypt(password);

		return userRepository.findByAccountAndPassword(account, encPassword);
	}

	public String setUserToSession(String account, String password) {
		Optional<User> userData = this.findByAccountAndPassword(account, password);
		String errorMessage = "";
		if (userData.isPresent()) {
			// Userデータが存在する場合はセッションに格納
			session.setAttribute("loginUser", userData.get());
		} else {
			System.out.println("値はありません");
			// 存在しない場合はエラーメッセージ
			errorMessage = "アカウントまたはパスワードが誤っています";
		}

		return errorMessage;
	}



}
