package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Message;
import com.example.demo.repository.MessageRepository;

@Service
public class MessageService {

	@Autowired
	MessageRepository messageRepository;

	// レコード全件取得(降順)
	public List<Message> findAllMessage() {
		return messageRepository.findAll(Sort.by(Sort.Direction.DESC, "createdDate"));
	}

	// レコード追加・編集
	public void saveMessage(Message message) {
		messageRepository.save(message);
	}

	//レコード削除
	public void deleteMessage(Integer id) {
		messageRepository.deleteById(id);
	}

	//レコード単件取得
	public Message editMessage(Integer id) {
		Message message = (Message) messageRepository.findById(id).orElse(null);
		return message;
	}

	// レコード検索
	public List<Message> findByCategory(String category) {
		String searcher = "%" + category + "%";
		return messageRepository.findByCategoryLike(searcher);
	}

}
