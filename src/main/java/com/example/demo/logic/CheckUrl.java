package com.example.demo.logic;

import java.util.List;
import java.util.Optional;

import com.example.demo.entity.User;

public class CheckUrl {

	public static List<String> typeCheck(List<String> errorMessages, Optional<String> id){
		if (!id.isPresent() || !id.get().matches("[+-]?\\d*(\\.\\d+)?")) {
			errorMessages.add("不正なパラメータが入力されました");
		}
		return errorMessages;
	}

	public static List<String> userCheck(List<String> errorMessages, Optional<User> userData){

		if(!userData.isPresent()) {
			errorMessages.add("不正なパラメータが入力されました");
		}

		return errorMessages;

	}
}
