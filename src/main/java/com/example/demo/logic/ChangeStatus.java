package com.example.demo.logic;

import com.example.demo.entity.User;

public class ChangeStatus {

	public static User statusChange(User user) {
		if(user.getIsStopped() == 0) {
			user.setIsStopped(1);
		}else {
			user.setIsStopped(0);
		}
		return user;
	}

}
