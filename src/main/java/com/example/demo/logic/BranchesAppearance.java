package com.example.demo.logic;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.example.demo.entity.Branch;

public class BranchesAppearance {


	public static Map<Integer, String> BranchesMap(List<Branch> allBranch) {

		Map<Integer, String> branchesMap = new LinkedHashMap<Integer, String>();

		for(Branch targetBranch :allBranch) {
			branchesMap.put(targetBranch.getId(),targetBranch.getName());
		}

		return branchesMap;
	}


}
