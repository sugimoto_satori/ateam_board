package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "comments")
@Getter
@Setter
public class Comment {
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotBlank(message = "コメントを入力してください")
	@Length(max = 500, message = "500文字以内で入力してください")
	@Column
	private String text;

	@Column(name = "user_id")
	private int userId;

	@Column(name = "message_id")
	private int messageId;

	@Column(name = "created_date", nullable = false, updatable = false, insertable = false)
	private Date createdDate;

	@Column(name = "updated_date", nullable = false, updatable = true, insertable = false)
	private Date updatedDate;

	// タスク更新時に更新時刻をupdatedDateに挿入
	@PreUpdate
	public void onPrePersist() {
		Date date = new Date();
		this.updatedDate = date;
	}
}
