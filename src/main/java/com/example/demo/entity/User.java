package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "users")
@Getter
@Setter
@DynamicUpdate
public class User {

	public static interface Signup {
	};

	public static interface EditUser {
	};

	public static interface Login {
	};

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotBlank(message = "アカウントを入力してください", groups = { Signup.class, EditUser.class })
	@NotBlank(message = "アカウントが入力されていません", groups = { Login.class })
	@Column
	private String account;

	@NotBlank(message = "パスワードを入力してください", groups = { Signup.class })
	@NotBlank(message = "パスワードが入力されていません", groups = { Login.class })
	@Column
	private String password;

	@NotBlank(message = "ユーザー名を入力してください", groups = { Signup.class, EditUser.class })
	@Column
	private String name;

	@Column(name = "branch_id")
	private int branchId;
	@Column(name = "department_id")
	private int departmentId;
	@Column(name = "is_stopped")
	private int isStopped;
	@Column(name = "created_date", nullable = false, insertable = false, updatable = false)
	private Date createdDate;
	@Column(name = "updated_date", nullable = false, insertable = false, updatable = true)
	private Date updatedDate;

	// タスク更新時に更新時刻をupdatedDateに挿入
	@PreUpdate
	public void onPrePersist() {
		Date date = new Date();
		this.updatedDate = date;
	}

	@AssertTrue(groups = { Signup.class, EditUser.class }, message = "支社と部署の組み合わせが不正です")
	public boolean isCombiValid() {
		if (branchId == 1) {

			if (departmentId > 2) {
				return false;
			}

		} else {

			if (departmentId < 3) {
				return false;
			}
		}
		return true;
	}

}
