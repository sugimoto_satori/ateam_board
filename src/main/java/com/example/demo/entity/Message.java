package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Entity
@Table(name = "messages")
public class Message {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	@NotBlank(message = "件名を入力してください")
	@Size(max = 30, message = "件名は30字以内で入力してください")
	private String title;
	@Column
	@NotBlank(message = "本文を入力してください")
	private String text;
	@Column
	@NotBlank(message = "カテゴリを入力してください")
	@Size(max = 10, message = "カテゴリは10字以内で入力してください")
	private String category;
	@Column(name="user_id")
	private int userId;
	@Column(name="created_date", insertable = false, updatable = false)
	private Date createdDate;
	@Column(name="updated_date")
	private Date updatedDate;

	@AssertTrue(message = "本文は1000字以内で入力してください")
	public boolean isTextlength() {

		int textLength = this.text.replaceAll("\\n", "").length();

		if(textLength <= 1000) {
			return true;
		}

		return false;
	}

}
