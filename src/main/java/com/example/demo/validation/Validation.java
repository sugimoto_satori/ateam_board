package com.example.demo.validation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.demo.entity.User;

public class Validation {

	public static List<String> passwordCheck(List<String> errorMessages,User user, String checkPassword) {

		String password = user.getPassword();

		if (!password.equals(checkPassword)) {
			errorMessages.add("入力したパスワードと確認用パスワードが一致しません");
		}

		return errorMessages;

	}

	public static List<String> lengthCheck(List<String> errorMessages,User user){
		Map<String,String> checkList = new HashMap<>();
		String password = user.getPassword();
		String account = user.getAccount();
		checkList.put("パスワード",password);
		checkList.put("アカウント",account);
		for(String target:checkList.keySet()) {
			int targetLength = checkList.get(target).length();
			if(targetLength>1 && targetLength<6) {
				errorMessages.add(target+"は6文字以上で入力してください");
			}
		}
		return errorMessages;
	}

}
