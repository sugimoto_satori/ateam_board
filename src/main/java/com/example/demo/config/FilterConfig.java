package com.example.demo.config;

import javax.servlet.Filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.filter.AdminFilter;
import com.example.demo.filter.LoginFilter;

@Configuration
public class FilterConfig {

	@Bean
	public FilterRegistrationBean loginFilterRegistration() {

	    FilterRegistrationBean registration = new FilterRegistrationBean();
	    registration.setFilter(loginFilter());
	    registration.addUrlPatterns("/", "/new", "/signup", "/user/*");
	    registration.addInitParameter("paramName", "paramValue");
	    registration.setName("LoginFilter");
	    registration.setOrder(1);
	    return registration;
	}

	@Bean(name = "LoginFilter")
	public Filter loginFilter() {
	    return new LoginFilter();
	}

	@Bean
	public FilterRegistrationBean adminFilterRegistration() {

	    FilterRegistrationBean registration = new FilterRegistrationBean();
	    registration.setFilter(adminFilter());
	    registration.addUrlPatterns("/signup", "/user/*");
	    registration.addInitParameter("paramName", "paramValue");
	    registration.setName("AdminFilter");
	    registration.setOrder(1);
	    return registration;
	}

	@Bean(name = "AdminFilter")
	public Filter adminFilter() {
	    return new AdminFilter();

	}

}
