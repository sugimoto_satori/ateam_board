package com.example.demo.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.example.demo.entity.User;

public class AdminFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
    		FilterChain chain) throws IOException, ServletException {

		HttpServletRequest castedRequest = (HttpServletRequest) request;
		HttpServletResponse castedResponse = (HttpServletResponse)response;
		HttpSession session = castedRequest.getSession();
		List<String> errorMessages = new ArrayList<>();

		User user = (User) session.getAttribute("loginUser");
		int branchId  = user.getBranchId();
		int departmentId = user.getDepartmentId();

		// loginuserのところは変更して返す。
		if (branchId != 1 || departmentId !=1) {

			errorMessages.add("権限がありません");
			session.setAttribute("adminErrors",errorMessages);
			castedResponse.sendRedirect("/");
			return;

		}

		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {

	}

}
