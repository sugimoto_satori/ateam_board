package com.example.demo.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.filter.GenericFilterBean;

import com.example.demo.entity.User;

public class LoginFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
    		FilterChain chain) throws IOException, ServletException {

		HttpServletRequest castedRequest = (HttpServletRequest) request;
		HttpServletResponse castedResponse = (HttpServletResponse)response;
		HttpSession session = castedRequest.getSession();
		List<String> errorMessages = new ArrayList<>();

		User user = (User) session.getAttribute("loginUser");
		// loginuserのところは変更して返す。
		if (user == null) {
			errorMessages.add("ログインしてください");
			session.setAttribute("loginErrors",errorMessages);
			castedResponse.sendRedirect("/login");
			return;
		}

        chain.doFilter(request, response);

    }

    @Override
    public void destroy() {

    }

}