package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.service.CommentService;

@Controller
public class CommentController {

	@Autowired
	CommentService commentService;
	@Autowired
	HttpSession session;

	// コメント投稿処理
	@PostMapping("/add-comment")
	public ModelAndView addComment(@Validated @ModelAttribute("commentModel") Comment comment, BindingResult result) {

		// バリデーションエラーがあった時の処理
		if (result.hasErrors()) {
			List<String> errorMessages = new ArrayList<String>();
			for (ObjectError error : result.getAllErrors()) {
				errorMessages.add(error.getDefaultMessage());
			}
			// エラー文をセッションに格納
			session.setAttribute("commentErrors", errorMessages);
			// コメント文をセッションに格納
			session.setAttribute("AddCommentText", comment.getText());
			session.setAttribute("AddCommentMessageId", comment.getMessageId());
			return new ModelAndView("redirect:/");
		}

		// 正常系処理
		// 投稿をテーブルに格納
		commentService.saveComment(comment);
		// セッションのエラーとコメント文を破棄
		session.removeAttribute("commentErrors");
		session.removeAttribute("AddCommentText");
		session.removeAttribute("AddCommentMessageId");
		// TOP画面へリダイレクト
		return new ModelAndView("redirect:/");
	}

	// コメント削除処理
	@DeleteMapping("/delete-comment/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		// idに紐づけられたコメントを削除
		commentService.deleteComment(id);
		// TOP画面へリダイレクト
		return new ModelAndView("redirect:/");
	}

}
