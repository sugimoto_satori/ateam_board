package com.example.demo.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Message;
import com.example.demo.service.CommentService;
import com.example.demo.service.MessageService;


@Controller
public class MessageController {

	@Autowired
	MessageService messageService;
	@Autowired
	CommentService commentService;

	// 投稿画面への遷移
	@GetMapping("/new")
	public ModelAndView newContent() {
		// mavの準備
		ModelAndView mav = new ModelAndView();

		// entityの準備
		Message message = new Message();

		// 画面遷移先の指定
		mav.setViewName("/new");

		// entityの保管
		mav.addObject("formModel", message);

		// 保管したmavを返す。
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@Validated @ModelAttribute("formModel") Message message,
			BindingResult result) {

		ModelAndView mav = new ModelAndView();

        if (result.hasErrors()) {
            List<String> errorMessages = new ArrayList<String>();
            for (ObjectError error : result.getAllErrors()) {
                errorMessages.add(error.getDefaultMessage());
            }
			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("/new");
			return mav;
		}

		// 投稿をDBに送信
		message.setUpdatedDate(new Date());
		messageService.saveMessage(message);

		// validationで設定したURLへ送信
		mav.setViewName("redirect:./");
		return mav;
	}

	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {

		//削除命令をDBに送信
		messageService.deleteMessage(id);

		//topへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 編集画面への遷移。
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {

		ModelAndView mav = new ModelAndView();

		//投稿をDBから取得
		Message message = messageService.editMessage(id);

		//編集する投稿をセット
		mav.addObject("formModel", message);

		//画面遷移先を指定
		mav.setViewName("/edit");
		return mav;
	}

	// 編集処理
	@PutMapping("/update")
	public ModelAndView updateContent (@Validated @ModelAttribute("formModel")
		Message message, BindingResult result) {

		ModelAndView mav = new ModelAndView();

        if (result.hasErrors()) {
            List<String> errorMessages = new ArrayList<String>();
            for (ObjectError error : result.getAllErrors()) {
                errorMessages.add(error.getDefaultMessage());
            }

			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("/edit");
			return mav;

		}

		//編集処理
		message.setUpdatedDate(new Date());
		messageService.saveMessage(message);

		// 前の画面へリダイレクト
		mav.setViewName("redirect:./");
		return mav;

	}

	// 検索ロジック
	@GetMapping("/search")
	public ModelAndView searchByDate(@RequestParam("start") String start, @RequestParam("end") String end,
			@RequestParam("category") String category) throws ParseException {

		ModelAndView mav = new ModelAndView();
		List<Message> messages = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");

		if(StringUtils.hasText(category)) {
			messages = messageService.findByCategory(category);
		} else {
			messages = messageService.findAllMessage();
		}

		if(!StringUtils.hasText(start)) {
       		start = "2000-01-01 00:00:00";
       	} else {
       		start += " 00:00:00";
    		mav.addObject("start", start.substring(0, 10));
       	}

		if(!StringUtils.hasText(end)) {
        	end = "2100-12-31 23:59:59";
        } else {
        	end += " 23:59:59";
    		mav.addObject("end", end.substring(0, 10));
        }

		Date since = sdf.parse(start);
		Date until = sdf.parse(end);

		// 全探索で日付を検索する。
		List<Message> searchedMessages = new ArrayList<>();

		for (int i = 0; i < messages.size(); i++) {

			// 検索対象の選出
			Message message = messages.get(i);
			Date createdDate = message.getCreatedDate();

			// 部分一致検索
				if (createdDate.compareTo(since) >= 0 && createdDate.compareTo(until) <= 0) {
					searchedMessages.add(message);

				}

		}

		List<Comment> commentData = commentService.findAllComment();

		mav.addObject("comments", commentData);
		mav.addObject("messages", searchedMessages);
		mav.addObject("category", category);
		mav.setViewName("/top");
		return mav;

	}
}
