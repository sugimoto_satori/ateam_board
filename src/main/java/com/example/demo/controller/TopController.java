package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Message;
import com.example.demo.service.CommentService;
import com.example.demo.service.MessageService;



@Controller
public class TopController {

	@Autowired
	MessageService messageService;
	@Autowired
	CommentService commentService;
	@Autowired
	HttpSession session;

	@GetMapping
	public ModelAndView top() {

		ModelAndView mav = new ModelAndView();

		// 投稿、返信を全件取得
		List<Message> messageData = messageService.findAllMessage();
		List<Comment> commentData = commentService.findAllComment();

		// entityの準備
		Comment commet = new Comment();

		// 画面遷移先を指定
		mav.setViewName("top");

		// 取得した投稿、返信データオブジェクトを保管

		List<String> errorMessages = new ArrayList<String>();
		session.setAttribute("loginErrors",errorMessages);
		mav.addObject("messages", messageData);
		mav.addObject("comments", commentData);
		mav.addObject("commentModel", commet);

		// 保管したmavを返す。
		return mav;
	}

}
