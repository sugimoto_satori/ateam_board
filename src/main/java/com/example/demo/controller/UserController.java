package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Branch;
import com.example.demo.entity.Department;
import com.example.demo.entity.User;
import com.example.demo.entity.User.EditUser;
import com.example.demo.entity.User.Login;
import com.example.demo.entity.User.Signup;
import com.example.demo.logic.ChangeStatus;
import com.example.demo.logic.CheckUrl;
import com.example.demo.logic.CipherUtil;
import com.example.demo.service.BranchService;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.UserService;
import com.example.demo.validation.Validation;

@Controller
public class UserController {

	@Autowired
	UserService userService;
	@Autowired
	BranchService branchService;
	@Autowired
	DepartmentService departmentService;
	@Autowired
	HttpSession session;

	//ユーザー登録画面表示処理
	@GetMapping("/signup")
	public ModelAndView viewSignup() {
		ModelAndView mav = new ModelAndView();

		List<String> errorMessages = new ArrayList<String>();
		String checkPassword = new String();
		User user = new User();

		List<Branch> allBranch = branchService.findAllBranches();
		List<Department> allDepartment = departmentService.findAllDepartment();

		mav.addObject("branches", allBranch);
		mav.addObject("departments", allDepartment);
		mav.addObject("formModel", user);
		mav.addObject("checkPassword", checkPassword);
		mav.addObject("errorMessages", errorMessages);
		mav.setViewName("/signup");
		return mav;
	}

	//ユーザー登録処理
	@PutMapping("/signup")
	public ModelAndView signup(@Validated(Signup.class) @ModelAttribute("formModel") User user, BindingResult result,
			String checkPassword) {
		ModelAndView mav = new ModelAndView();
		List<Branch> allBranch = branchService.findAllBranches();
		List<Department> allDepartment = departmentService.findAllDepartment();

		List<String> errorMessages = new ArrayList<String>();
		for (ObjectError error : result.getAllErrors()) {
			errorMessages.add(error.getDefaultMessage());
		}
		Optional<User> registeredUser = userService.findByAccount(user.getAccount());

		//バリデーションを行いviewをセットする処理
		errorMessages = Validation.passwordCheck(errorMessages, user, checkPassword);
		errorMessages = Validation.lengthCheck(errorMessages, user);

		if (registeredUser.isPresent()) {
			errorMessages.add("アカウントが重複しています");
		}

		mav.addObject("branches", allBranch);
		mav.addObject("departments", allDepartment);
		mav.addObject("errorMessages", errorMessages);

		//ユーザー登録処理
		if (errorMessages.isEmpty()) {
			userService.saveUser(user);
			mav.setViewName("redirect:/user/management");
			return mav;
		}

		mav.setViewName("/signup");

		return mav;
	}

	//ユーザー編集画面表示処理
	@GetMapping({ "/user/edit/", "/user/edit/{id}" })
	public ModelAndView viewEditUser(@PathVariable(name = "id", required = false) Optional<String> id) {

		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();

		List<Branch> allBranch = branchService.findAllBranches();
		List<Department> allDepartment = departmentService.findAllDepartment();

		//URLチェック
		//1,idが数字かのチェック
		errorMessages = CheckUrl.typeCheck(errorMessages, id);
		if (errorMessages.isEmpty()) {

			//2,idが存在するかのチェック
			Optional<User> userData = userService.userFindById(Integer.parseInt(id.get()));
			errorMessages = CheckUrl.userCheck(errorMessages, userData);

			//エラーがない時はデータをセットしユーザー編集画面を表示
			if (errorMessages.isEmpty()) {
				mav.addObject("branches", allBranch);
				mav.addObject("departments", allDepartment);
				mav.addObject("formModel", userData.get());
				mav.addObject("errorMessages", errorMessages);
				mav.setViewName("/edit-user");
				return mav;
			}
		}

		//エラーが存在する場合はエラーメッセージをセットしTOP画面を表示
		session.setAttribute("userErrors", errorMessages);
		mav.setViewName("redirect:/user/management");

		return mav;
	}

	//ユーザー編集処理
	@PutMapping("/user/edit{id}")
	public ModelAndView editUuser(@Validated(EditUser.class) @ModelAttribute("formModel") User user,
			BindingResult result, String checkPassword, String blankPassword) {
		ModelAndView mav = new ModelAndView();
		List<Branch> allBranch = branchService.findAllBranches();
		List<Department> allDepartment = departmentService.findAllDepartment();

		//エラーメッセージの設定
		List<String> errorMessages = new ArrayList<String>();
		for (ObjectError error : result.getAllErrors()) {
			errorMessages.add(error.getDefaultMessage());
		}
		Optional<User> registeredUser = userService.findByAccount(user.getAccount());
		Optional<User> currentUser = userService.userFindById(user.getId());

		//バリデーションを行いviewをセットする処理
		errorMessages = Validation.passwordCheck(errorMessages, user, checkPassword);
		errorMessages = Validation.lengthCheck(errorMessages, user);

		if (registeredUser.isPresent()) {
			if (!currentUser.get().getAccount().equals(user.getAccount())) {
				errorMessages.add("アカウントが重複しています");
			}
		}

		if (user.getPassword().isEmpty()) {
			user.setPassword(blankPassword);
		} else {
			//パスワードの再設定
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);
		}

		mav.addObject("branches", allBranch);
		mav.addObject("departments", allDepartment);
		mav.addObject("errorMessages", errorMessages);
		//ユーザー登録処理
		if (errorMessages.isEmpty()) {
			userService.saveUser(user);
			mav.setViewName("redirect:/user/management");
			return mav;
		}

		mav.setViewName("/edit-user");

		return mav;
	}

	//ユーザー管理画面表示処理
	@GetMapping("/user/management")
	public ModelAndView ViewUserManagement() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user-management");
		List<User> allUser = userService.findByUser();
		mav.addObject("users", allUser);

		return mav;
	}

	//ステータス変更処理
	@PutMapping("/status/{id}")
	public ModelAndView updateStatus(@ModelAttribute("user") User user) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/user/management");

		//isStoppedのステータスの変更処理
		User userChangedStatus = ChangeStatus.statusChange(user);

		//ユーザー登録処理
		userService.saveUser(userChangedStatus);
		return mav;
	}

	// ログイン画面表示処理
	@GetMapping("/login")
	public ModelAndView ViewLogin() {
		ModelAndView mav = new ModelAndView();

		User user = new User();
		mav.addObject("formModel", user);
		mav.setViewName("/login");
		return mav;
	}

	// ユーザーのログイン処理
	@PostMapping("/login")
	public ModelAndView Login(@Validated(Login.class) @ModelAttribute("formModel") User user, BindingResult result) {
		ModelAndView mav = new ModelAndView();
		// エラーメッセージを格納するListを作成
		List<String> errorMessages = new ArrayList<String>();

		// バリデーションエラーがあった時の処理
		if (result.hasErrors()) {
			for (ObjectError error : result.getAllErrors()) {
				errorMessages.add(error.getDefaultMessage());
			}

			mav.addObject("errorMessages", errorMessages);
			mav.addObject("formModel", user);
			mav.setViewName("/login");
			return mav;
		}

		// 取得したアカウント名＆パスワードでDBデータを取得し、セッションに格納しnullを返却
		// データを取得できなかった場合エラー文を返却
		String notUserErrorMessage = userService.setUserToSession(user.getAccount(), user.getPassword());

		// エラーメッセージが存在した場合の処理
		if (StringUtils.hasText(notUserErrorMessage)) {
			errorMessages.add(notUserErrorMessage);
			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("/login");
			return mav;
		}

		return new ModelAndView("redirect:/");
	}

	// ユーザーのログアウト処理
	@PostMapping("/logout")
	public ModelAndView Logout() {
		session.invalidate();
		return new ModelAndView("redirect:/");
	}

}
