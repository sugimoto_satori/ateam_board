$(function() {
	//確認ダイヤログを表示する処理

	$('.status-change, .delete-comment, .delete-message').on('click', function() {
		console.log($(this).attr('class'));
		let messageLog;
		switch($(this).attr('class')){
		case 'status-change':
			messageLog =$(this).val()+'してもよろしいですか？';
			break;

		case 'delete-comment':
		case 'delete-message':
			messageLog ='削除してもよろしいですか？';
		}

		let result = confirm(messageLog);

		if(result){
			return true;
		}else{
			return false;
		}
	});

});