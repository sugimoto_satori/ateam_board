INSERT INTO branches (
    name
) values (
    '本社'
), (
    'A支社'
), (
    'B支社'
), (
    'C支社'
);

INSERT INTO departments (
    name
) values (
    '総務人事部'
), (
    '情報管理部'
), (
    '営業部'
), (
    '技術部'
);